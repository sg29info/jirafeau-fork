<h1>That's a full copy of Jirafeau version 4.1.1 by Jérôme Jutteau.</hi>

Just wast to much time to understand how git works so make my own copy.

<b>Just skip this commit cause it's a major change :</b>
```diff --git a/lib/functions.php b/lib/functions.php
index 2817aa1..df207da 100644
--- a/lib/functions.php
+++ b/lib/functions.php
@@ -25,9 +25,16 @@
  */
 function s2p($s)
 {
+    $block_size = 8;
     $p = '';
     for ($i = 0; $i < strlen($s); $i++) {
-        $p .= $s{$i} . '/';
+        $p .= $s{$i};
+        if (($i + 1) % $block_size == 0) {
+            $p .= '/';
+        }
+    }
+    if (strlen($s) % $block_size != 0) {
+        $p .= '/';
     }
     return $p;
 }